import turtle as tu
import pyautogui as py

#ตัว T
tu.penup()
tu.goto(-200,200)
tu.pendown()
tu.pensize(8)
tu.color('red')
tu.goto(-300,200)
tu.penup()
tu.goto(-250,200)
tu.pendown()
tu.pensize(8)
tu.goto(-250,-8)

#ตัวโอ
tu.pensize(8)
tu.color('blue')
tu.penup()
tu.goto(-120,20)
tu.pendown()
tu.circle(80)

#ตัว N
tu.penup()
tu.goto(0,0)
tu.pendown()
tu.goto(0,200)
tu.goto(40,200)
tu.goto(80,0)
tu.goto(120,0)
tu.goto(120,200)

#ตัว E
tu.penup()
tu.goto(240,200)
tu.pendown()
tu.goto(140,200)
tu.goto(140,100)
tu.goto(230,100)
tu.goto(140,100)
tu.goto(140,0)
tu.goto(240,0)

#ขีดเส้นใต้
tu.penup()
tu.goto(-250,-80)
tu.pendown()
tu.color('green')
tu.pensize(14)
tu.goto(260,-20)
