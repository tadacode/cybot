# มาเรียนรู้กับ Teachabel Machine ดีกว่า 
- [ฝึกสอนคอมพิวเตอร์ให้จำหน้าตา](https://teachablemachine.withgoogle.com/)
- [เวปไซต์ฝึก Machine Learning บันทึกท่าทาง 3 ท่าทาง แล้วให้ทำงาน เช่นเปิดเสียงนี้ พูดเสียงนี้ ท่าทางนี้ให้จับคู่กับการทำงานอะไร ลองเล่นกันเลย](https://teachablemachine.withgoogle.com/v1/)
- [MIT Animate เขียนโปรแกรมแบบลากวาง](https://mitmedialab.github.io/prg-extension-boilerplate/create/)
# Cybot

[ฟังค์ชั่นควบคุมเม้าส์ คีย์บอร์ด](https://pyautogui.readthedocs.io/en/latest/keyboard.html#the-press-keydown-and-keyup-functions)
<br>[Autobot windows สร้างบอตไว้ทำงานแทนเราในวินโดว์ทำงานเหมือนเราใช้ เมาส์ คีย์บอร์ด ด้วย pyautogui](https://phyblas.hinaboshi.com/20170101)
<br>[รวมคำสั่ง python ที่ใช้จัดการกับข้องความ string](https://www.dcrub.com/python-strings)
<br>[งานที่ต้องกลับมาทำต่อ](https://colab.research.google.com/drive/1F_m1Yz3XEYdUm5A_YfQvf5LIoEW6joGS#scrollTo=JevWDWMmw9NI)
<br> ต้องการค้นหาคำว่า กฤติกา โพชารี ,ว่ามีกี่ตัว,และก๊อปปี้ลิ้งค์ที่เชื่อมโยงกับชื่อนี้มาเก็บไว้ใน โน๊ต
<br> คำสั้่งเขียนเต่า turtle [turtle pypi](https://docs.python.org/3/library/turtle.html``)
<br> คำสั่งเขียนวงกลม pen = turtle.circle(50)
## โปรเจ็ค Colab น่าสนใจ
<br> [Colab จับภาพกล้องวงจรปิด](https://www.youtube.com/watch?v=10joRJt39Ns)
